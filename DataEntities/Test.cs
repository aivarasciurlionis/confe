﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataEntities
{
    public class Test
    {
        public Test()
        {
            Questions = new List<Question>();
        }

        [Key]
        public int Id { get; set; }
        public int QuestionCount { get; set; }
        [MaxLength(128)]
        public string Name { get; set; }
        public ICollection<Question> Questions { get; set; }
        [JsonIgnore]
        public virtual User User { get; set; }
    
    }
}
