namespace DataEntities.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<DataEntities.DataContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        public void SeedUsers(ref DataContext context)
        {
            if (!context.Roles.Any(r => r.Name == "Admin"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "Admin" };

                manager.Create(role);
            }

            if (!context.Roles.Any(r => r.Name == "User"))
            {
                var store = new RoleStore<IdentityRole>(context);
                var manager = new RoleManager<IdentityRole>(store);
                var role = new IdentityRole { Name = "User" };
                manager.Create(role);            
            }

            if (!context.Users.Any(u => u.UserName == "admin@confe.com"))
            {
                var store = new UserStore<User>(context);
                var manager = new UserManager<User>(store);
                var user = new User { UserName = "admin@confe.com", Email = "admin@confe.com" };

                manager.Create(user, "Password@1");                
                manager.AddToRole(user.Id, "Admin");
            }

        }

      

        protected override void Seed(DataEntities.DataContext context)
        {
           // SeedUsers(ref context);
            context.SaveChanges();
        }
    }
}
