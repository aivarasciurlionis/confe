﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataEntities
{
    public class QuestionView
    {     
        public int Id { get; set; }
        [MaxLength(512)]
        public string QuestionName { get; set; }
        public DataEntities.Question.QuestionTyp QuestionType { get; set; }
        public DateTime? LastAsked { get; set; }
        public int AnswerCount { get; set; }     
        public int CurrentAnswers { get; set; }
        public int TotalAnswers { get; set; }
    }
}
