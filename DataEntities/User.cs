﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace DataEntities
{
    public class User : IdentityUser
    {
        public User()
        {
            Tests = new List<Test>();
            Questions = new List<Question>();
        }

        [MaxLength(30)]
        public string FirstName { get; set; }
        [MaxLength(30)]
        public string LastName { get; set; }
        [MaxLength(512)]
        public string Biography { get; set; }
        [MaxLength(60)]
        public string Occupation { get; set; }
        public bool ShowResultsForParticipants { get; set; }
        public bool HasActiveQuestion { get; set; }
        public int ActiveQuestionId { get; set; }
        public int DefaultConferenceTime { get; set; }
        public string ConferenceKey { get; set; }
        public DateTime? RegisterDate { get; set; }
        public bool ShowingResults { get; set; }
        public int ResultQuestionId { get; set; }
        public ICollection<Test> Tests { get; set; }
        public ICollection<Question> Questions { get; set; }
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }
    }

}
