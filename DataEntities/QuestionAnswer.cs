﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataEntities
{
    public class QuestionAnswer
    {
        [Key]
        public int Id { get; set; }
        [MaxLength(512)]
        public string Name { get; set; }
        public int CurrentAnswersCount { get; set; }
        public int TotalAnswersCount { get; set; }
        [JsonIgnore]
        public virtual Question Question { get; set; }
    }
}
