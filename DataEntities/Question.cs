﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataEntities
{
    public class Question
    {
        public enum QuestionTyp  {Single, Multiple, Freeform};
        public Question()
        {
            QuestionAnswers = new List<QuestionAnswer>();
        }
        [Key]
        public int Id { get; set; }
        [MaxLength(512)]
        public string QuestionName { get; set; }
        public QuestionTyp QuestionType { get; set; }
        public DateTime? LastAsked { get; set; }
        public DateTime? Expires { get; set; }
        public ICollection<QuestionAnswer> QuestionAnswers { get; set; }
        [JsonIgnore]
        public virtual User User { get; set; }
        public int CurrentAnswers { get; set; }
        public int TotalAnswers { get; set; }
        public int AbsoluteTotalAnswers { get; set; }
    }
}
