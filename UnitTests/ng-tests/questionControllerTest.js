﻿describe('Controller: questionsController', function () {
    beforeEach(module('confe'));
    var Controller, scope;
    beforeEach(inject(function ($controller) {
        scope = {};
        Controller = $controller('questionsController', {
            $scope : scope
        });
    }));
    it('should have scope & controller defined', function () {
        expect(scope).toBeDefined();
        expect(Controller).toBeDefined();
    });

    it('should correctly change enum to string', function () {
        Controller.question = { questionType: 'One possible answer' };
        Controller.stringToEnum();
        expect(Controller.question.questionType).toBe(0);
        Controller.question = { questionType: 'Multiple answers' };
        Controller.stringToEnum();
        expect(Controller.question.questionType).toBe(1);
    });

    it('should correctly change string to enum', function () {
        Controller.question = { questionType: 0 };
        Controller.enumToString();
        expect(Controller.question.questionType).toBe('One possible answer');
        Controller.question = { questionType: 1 };
        Controller.enumToString();
        expect(Controller.question.questionType).toBe('Multiple answers');
    });


    it('should add empty answer to possible ansers array', function () {
        Controller.question = { questionAnswers: [] };
        Controller.ansC = 0;
        Controller.addAnswer();
        expect(Controller.ansC).toBe(1);
        expect(Controller.question.questionAnswers.length).toBe(1);
        expect(Controller.question.questionAnswers[0].name).toBe('');
        Controller.addAnswer();
        Controller.addAnswer();
        Controller.addAnswer();
        expect(Controller.ansC).toBe(4);
        expect(Controller.question.questionAnswers.length).toBe(4);
        expect(Controller.question.questionAnswers[3].name).toBe('');
    });

    it('should remove possible answer by its id', function () {
        Controller.question = { questionAnswers: [{ name: '1' }, { name: '2' }, { name: '3' }, { name: '4' }] };
        Controller.removeAnswer(2);
        expect(Controller.question.questionAnswers.length).toBe(3);
        expect(Controller.question.questionAnswers).toEqual([{ name: '1' }, { name: '2' }, { name: '4' }]);
        Controller.removeAnswer(0);
        expect(Controller.question.questionAnswers.length).toBe(2);
        expect(Controller.question.questionAnswers).toEqual([{ name: '2' }, { name: '4' }]);
        Controller.removeAnswer(1);
        Controller.removeAnswer(0);
        expect(Controller.question.questionAnswers.length).toBe(0);
        expect(Controller.question.questionAnswers).toEqual([]);
    });

});