﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Confe.Controllers
{
    public class QuestionsController : Controller
    {
        // GET: Questions
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult New()
        {
             return View();
        }

        public ActionResult NewTest()
        {
            return View();
        }

        public ActionResult EditTest(int id)
        {
            ViewBag.Id = id;
            return View();
        }

        public ActionResult Edit(int id)
        {
            ViewBag.Id = id;
            return View();
        }

        public ActionResult History(int id)
        {
            ViewBag.Id = id;
            return View();
        }

    }
}