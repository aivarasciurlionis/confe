﻿angular.module('confe').factory('authInterceptor', function ($rootScope, $q, $window, $location) {
    return {
        request: function (config) {
            config.headers = config.headers || {};
            if ($window.localStorage.access_token) {
                config.headers.Authorization = 'Bearer ' + $window.localStorage.access_token;
            }
            return config;
        },
        responseError: function(response) {
            if (response.status === 401) {
                swal({
                    title: "Unauthorized",
                    text: "You will be redirected to login page",
                    type: "warning",
                    showCancelButton: false,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Login",
                    closeOnConfirm: false
                }, function () { $window.location = "/Authorize/Login"; });
                return response;
            }
            return $q.reject(response);
        }
    };
});

angular.module('confe').config(function ($httpProvider) {
    $httpProvider.interceptors.push('authInterceptor');
});