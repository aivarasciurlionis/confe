﻿function authorizeController($http, $window, $scope, apiUrl) {

    var self = this;

    self.loggedin = !!$window.localStorage.access_token;
    self.itemsPerPage = 8;
    self.currentPage = 0;
    self.itemscount = 0;


    self.loginModel = {
        Email: '',
        Password: '',
        ConfirmPassword: ''
    };

    self.getAllUsers = function () {    
            $http.get(apiUrl + "api/Admin/AllUsers").then(function (response) {
                self.allUsers = response.data;
            }, function (response) {
                swal(":( something went wrong", response.data.message + '\n ------ \n' + response.data.exceptionType + '\n ----- \n' + response.data.exceptionMessage, 'warning');
            });        
    }


    self.logout = function (redirect) {

        $http({
            method: 'post',
            url: apiUrl + "api/Account/Logout",
        }).then(function () {
            localStorage.setItem('access_token', '');
            if (redirect) { $window.location = '/Authorize/Login' }
        });
    }

    self.newKey = function () {
        $http.get(apiUrl + "api/Conference/UniqueKey")
              .then(function ($response) {
                  self.getCurrentUser();
              }, function (response) {
                  swal(":( Something went wrong", JSON.stringify(data), "error");
              });
        }

    self.getCurrentUser = function () {
        self.loading = true;
        $http.get(apiUrl + "api/Account/CurrentUser")
        .then(function ($response) {
            self.user = $response.data;
            self.loading = false;
        }, function (response) {
            swal(":( something went wrong", response.data.message + '\n ------ \n' + response.data.exceptionType + '\n ----- \n' + response.data.exceptionMessage, 'warning');
        });
    }


    self.save = function () {
        self.loading = true;
        $http({
            method: 'put',
            url: apiUrl + "api/Account/UpdateCurrentUser",
            data  : self.user
        }).then(function () {
            self.getCurrentUser();
        });
    }


    self.register = function () {
        self.loading = true;
        $http({
            method: 'POST',
            url: apiUrl + "/api/Account/Register",
            contentType: 'application/json',
            data: self.loginModel
        }).then(function(){
            $window.location.href = '/Authorize/Login';

        }, function (response) {
            swal(":( Something went wrong", response.data.error + '\n' + response.data.error_description, "error");
        }
        )


    }

    self.enterConference = function () {
        $http({
            method: 'GET',
            url: apiUrl + "api/Conference/Exists?key=" + self.conferenceKey
        }).then(function (response) {
            $window.localStorage["conferenceKey"] = self.conferenceKey;
            $window.location.href = '/Conference/InProgress';
        }, function (reason) {
            swal("Error", reason.data.message, "error");
        });
    }



    self.login = function (admin) {
        self.loading = true;

        var data = "grant_type=password&username=" + self.loginModel.Email + "&password=" + self.loginModel.Password;
        $http({
            method: 'POST',
            url: apiUrl + "Token",
            contentType: 'application/x-www-form-urlencoded',
            data: data
        }).then(function (response) {
            $window.localStorage.access_token = response.data.access_token;
            if (admin) {
                $window.location = "/Admin";
            } else {
                $window.location = "/";
            }
        }).catch(function (response) {
            swal("Email or password incorect", "Check your credentials and try again", "error");
            self.loading = false;
        });
    }


}
angular.module('confe')
 .controller('authorizeController', authorizeController);