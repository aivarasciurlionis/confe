﻿function conferenceController($http, $window, $scope, apiUrl, $interval, $q) {
    var self = this;

    self.questionTypes = ['One possible answer', 'Multiple answers', 'Freeform answer'];
    self.ansC = 0;
    self.loading = false;
    self.questionId = 0;
    self.selectedQuestion = null;
    self.started = false;
    self.gotAnswers = 0;
    self.stopped = false;
    self.gotResults = false;
    self.freeform = '';
    self.data = [];
    self.labels = [];

    self.chartType = 0
    self.series = ['Data'];
    self.chartTypes = ['Bar', 'Pie', 'Radar', 'PolarArea', 'Doughnut', 'Line'];

    self.labels = ['2006', '2007', '2008', '2009', '2010', '2011', '2012'];
    self.series = ['Series A'];

    self.conferenceStates = ['Waiting for question...', 'Submiting', 'Waiting for results...', 'Waiting for question to finish']
    self.currentState = 0;

    self.currentStateUser = 0;

    self.questionText = self.conferenceStates[0];

    self.currentQuestionId = 0;

    self.nextType = function () {
        self.chartType += 1;
        if (self.chartType >= self.chartTypes.length)
            self.chartType = 0;
    }

    self.checkForActive = function () {
        $interval(function () {

            switch (self.currentState) {
                case (0):
                    $http({
                        method: 'GET',
                        url: apiUrl + 'api/Conference/ActiveQuestions?key=' + $window.localStorage["conferenceKey"],
                    }).then(function (resp) {
                        if (resp.data === "No active questions" || resp.data === "Question has expired") {
                            self.question = null;
                        }
                        else if (resp.data === "Conference key has expired") {
                            $window.location.href = "/Authorize/Login";
                        }
                        else {
                            self.question = resp.data;
                            self.questionText = self.question.questionName;
                            self.gotResults = false;
                            self.currentState = 1;
                        }
                        self.loading = false;
                    });
                    break;
                case (1):
                    break;
                case (2):
                    $http({
                        method: 'GET',
                        url: apiUrl + 'api/Conference/ResultsAnon?key=' + $window.localStorage["conferenceKey"],
                    }).then(function (resp) {
                        if (resp.data != "No results available") {
                            self.questionResults = resp.data;
                            self.parseResults(false);
                            self.questionText = self.conferenceStates[0];
                            self.currentState = 0;
                            self.gotResults = true;
                        }
                    });
                    break;
                case (3):
                    $http({
                        method: 'GET',
                        url: apiUrl + 'api/Conference/ActiveQuestions?key=' + $window.localStorage["conferenceKey"],
                    }).then(function (resp) {
                        if (resp.data === "No active questions" || resp.data === "Question has expired") {
                            self.currentState = 0;
                            self.questionText = self.conferenceStates[0];

                        }
                    });
                    break;
            }
        }, 2000);
    }

    self.submit = function () {
        var atLeastOne = false;
        var data = {};
        if (self.question.questionType == 0) {
            for (var i = 0; i < self.question.questionAnswers.length; i++) {
                if (self.question.questionAnswers[i].selected) {
                    data.singleId = self.question.questionAnswers[i].id;
                    atLeastOne = true;
                }
            }
        }

        if (self.question.questionType == 1) {
            data.multipleIds = [];
            for (var i = 0; i < self.question.questionAnswers.length; i++) {
                if (self.question.questionAnswers[i].selected) {
                    data.multipleIds.push(self.question.questionAnswers[i].id);
                    atLeastOne = true;
                }
            }
        }

        if (self.question.questionType == 2) {
            if (self.freeform.length > 0) {
                data.freeform = self.freeform;
                atLeastOne = true;
            }
        }


        if (!atLeastOne) {
            swal("Error", "Choose at least one answer", "error");
        }
        else {
            console.log(data);
            $http({
                method: 'POST',
                url: apiUrl + 'api/Conference/SubmitAnswer?key=' + $window.localStorage["conferenceKey"],
                data: data
            }).then(function (resp) {
                if (resp.data === true)
                    self.currentState = 2;
                else
                    self.currentState = 3;

                self.questionText = self.conferenceStates[self.currentState];

            }, function (resp) {
                self.currentState = 0;
                self.questionText = self.conferenceStates[self.currentState];
            });


        }

    }

    self.nextQuestionInTest = function () {
        self.currentStateUser = 1;
        self.stopped = false;
        self.currentQuestionId++;
        if (self.currentQuestionId < self.test.questionCount) {
            self.getDefaults().then(function () {
                self.activateQuestion(false);
            });         
        } else {
            self.reset();
        }
    }

    self.reset = function () {
        self.getAllQuestions();
        self.getDefaults();
        self.currentStateUser = 0;
        self.stopped = false;
    }

    self.selectAnswer = function (qa) {
        if (self.question.questionType == 1) {
            if (qa.selected)
                qa.selected = false;
            else
                qa.selected = true;
        }
        else if (self.question.questionType == 0) {
            if (qa.selected)
                qa.selected = false;
            else {
                for (var i = 0; i < self.question.questionAnswers.length; i++) {
                    self.question.questionAnswers[i].selected = false;
                }
                qa.selected = true;
            }
        }
    }

    self.multipleData = function () {
        switch (self.chartType) {
            case (0): return true;
            case (1): return false;
            case (2): return true;
            case (3): return false;
            case (4): return false;
            case (5): return true;
        }
    }

    self.getCurrentUser = function () {
        self.loading = true;
        $http.get(apiUrl + "api/Account/CurrentUser")
        .then(function ($response) {
            self.user = $response.data;
            self.loading = false;
        }, function (response) {

        });
    }

    self.backToMenu = function () {
        $window.location.href = "/Questions";
    }

    self.getHistory = function (id) {
        self.questionId = id;
        self.loading = true;
        self.question = {};
        $http({
            method: 'GET',
            url: apiUrl + 'api/Question/' + id,

        }).then(function (resp) {
            self.question = resp.data;
            self.enumToString();
            self.loading = false;
            self.questionResults = self.question;
            self.parseResults(true);
        });


    }

    self.parseResults = function (total) {
        self.labels = [];
        self.data = [];
        self.mulData = [];
        for (var i = 0; i < self.questionResults.questionAnswers.length; i++) {
            self.labels.push(self.questionResults.questionAnswers[i].name);
            if (total) {
                self.data.push(self.questionResults.questionAnswers[i].totalAnswersCount);
            } else
                self.data.push(self.questionResults.questionAnswers[i].currentAnswersCount);
        }

        self.mulData.push(self.data);

    }

    self.getResults = function () {
        self.questionResults = {};
        $http({
            method: 'GET',
            url: apiUrl + 'api/Conference/ResultsUser',
        }).then(function (resp) {
            self.questionResults = resp.data;
            self.parseResults(false);
        });

    }

    self.checkResults = function () {
        $interval(function () {

            switch (self.currentStateUser) {
                case (0): break;
                case (1):
                    if (self.userDefaults.defaultConferenceTime > 0 && !self.stopped) {
                        $http({
                            method: 'GET',
                            url: apiUrl + 'api/Conference/CurrentAnswers',
                        }).then(function (resp) {
                            self.gotAnswers = resp.data;
                            self.loading = false;
                        });
                    }
                    else if (!self.stopped) {
                        self.stopTimer();

                    }
                    break;
                case (2): break;
            }
        }, 3000);
    }

    self.getTest = function (id) {
        self.questionId = id;
        var defered = $q.defer();
        self.loading = true;
        self.question = {};
        $http({
            method: 'GET',
            url: apiUrl + 'api/Tests/' + id,

        }).then(function (resp) {
            self.test = resp.data;
            defered.resolve(resp);
            self.loading = false;
        });
        return defered.promise;
    }

    self.start = function (single) {
        if (!single) {
            self.currentQuestionId = 0;
            self.saveDefaults().then(function () {
                self.getTest(self.selectedTest.id).then(function () {
                    self.activateQuestion(single);
                });              
            });
        }
        else {
            self.saveDefaults().then(function () {
                self.activateQuestion(single);
            });
        }
    }

    self.stopTimer = function () {
        $http({
            method: 'POST',
            url: apiUrl + 'api/Conference/DeactivateLast',
        }).then(function (resp) {
            self.stopped = true;
            self.getResults();
            self.currentStateUser = 2;
            self.loading = false;
        });
    }


    self.countTime = function () {
        $interval(function () {
            if (self.userDefaults.defaultConferenceTime > 0 && self.currentStateUser === 1) {
                self.userDefaults.defaultConferenceTime -= 1;
            }
        }, 1000);
    }


    self.getDefaults = function () {
        var defered = $q.defer();
        $http({
            method: 'GET',
            url: apiUrl + 'api/Conference/UserDefaults',

        }).then(function (resp) {
            self.userDefaults = resp.data;
            defered.resolve(resp);
            self.loading = false;
        });
        return defered.promise;
    }


    self.activateQuestion = function (single) {
        if (single && self.selectedQuestion) {
            $http({
                method: 'POST',
                url: apiUrl + 'api/Conference/MakeActive?id=' + self.selectedQuestion.id

            }).then(function (resp) {
                self.started = true;
                self.checkResults();
                if (!self.counting)
                    self.countTime();

                self.counting = true;


                self.currentStateUser = 1;
                self.getQuestion(self.selectedQuestion.id);

                self.loading = false;
            }, function (reason) {

            });
        }
        if (!single) {
            console.log(self.selectedTest);
            $http({
                method: 'POST',
                url: apiUrl + 'api/Conference/MakeActive?id=' + self.test.questions[self.currentQuestionId].id

            }).then(function (resp) {
                self.started = true;
                self.checkResults();
                if (!self.counting)
                    self.countTime();

                self.counting = true;


                self.currentStateUser = 1;
                self.getQuestion(self.test.questions[self.currentQuestionId].id);

                self.loading = false;
            }, function (reason) {

            });


        }


    }

    self.saveDefaults = function () {
        var defered = $q.defer();
        $http({
            method: 'PUT',
            url: apiUrl + 'api/Conference/UserDefaults',
            data: self.userDefaults

        }).then(function (resp) {
            defered.resolve(resp);
            self.loading = false;
        });
        return defered.promise;
    }

    self.question =
    {
        questionName: '',
        questionType: 0,
        questionAnswers: []
    }

    self.enumToString = function () {
        switch (self.question.questionType) {
            case 0: self.question.questionType = 'One possible answer'; break;
            case 1: self.question.questionType = 'Multiple answers'; break;
            case 2: self.question.questionType = 'Freeform answer'; break;
        }
    }

    self.stringToEnum = function () {
        switch (self.question.questionType) {
            case 'One possible answer': self.question.questionType = 0; break;
            case 'Multiple answers': self.question.questionType = 1; break;
            case 'Freeform answer': self.question.questionType = 2; break;
        }
    }

    self.getQuestion = function (id) {
        self.questionId = id;
        self.loading = true;
        self.question = {};
        $http({
            method: 'GET',
            url: apiUrl + 'api/Question/' + id,

        }).then(function (resp) {
            self.question = resp.data;
            self.enumToString();
            self.loading = false;
        });
    }

    self.selectQuestion = function (q) {
        alert(1);
        self.selectedQuestion = q;
    }


    self.getAllQuestions = function () {
        self.loading = true;
        $http({
            method: 'GET',
            url: apiUrl + 'api/Questions',

        }).then(function (response) {
            self.questions = response.data;
            self.loading = false;
        })
    }

    self.getAllTests = function () {
        self.loading = true;
        $http({
            method: 'GET',
            url: apiUrl + 'api/Tests',

        }).then(function (response) {
            self.tests = response.data;
            self.loading = false;
        })
    }

    self.confirmDeletion = function (callback, id) {
        swal({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonColor: "#5EA1CE",
            confirmButtonText: "Yes",
            closeOnConfirm: true
        }, function () {
            callback(id)
        });
    }



}
angular.module('confe')
 .controller('conferenceController', conferenceController);