﻿function questionsController($http, $window, $scope, apiUrl) {
    var self = this;

    self.questionTypes = ['One possible answer', 'Multiple answers', 'Freeform answer'];
    self.ansC = 0;
    self.loading = false;
    self.questionId = 0;
    self.testId;

    self.question =
    {
        questionName: '',
        questionType: 0,
        questionAnswers: []
    }


    self.goToEditTest = function (id) {
        $window.location.href = "/Questions/EditTest/"+id;
    }

    self.goToNew = function () {
        $window.location.href = "/Questions/New";
    }

    self.goToNewTest = function () {
        $window.location.href = "/Questions/NewTest";
    }

    self.backToMenu = function () {
        $window.location.href = "/Questions";
    }

    self.addAnswer = function () {
        self.ansC += 1;
        var emptyAnswer = {};
        emptyAnswer.name = '';
        self.question.questionAnswers.push(emptyAnswer);
    }

    self.removeAnswer = function(id){
        self.question.questionAnswers.splice(id, 1);
    }

    self.deleteQuestion = function (id) {
        self.loading = true;
        $http({
            method: 'Delete',
            url: apiUrl + 'api/Question/' + id,
        }).then(function (resp) {
            swal("Deleted!", "", "success");
            self.getAllQuestions();
            self.loading = false;
        }); 
    }

    self.deleteTest = function (id) {
        self.loading = true;
        $http({
            method: 'Delete',
            url: apiUrl + 'api/Tests/' + id,
        }).then(function (resp) {
            swal("Deleted!", "", "success");
            self.getAllTests();
            self.loading = false;
        });
    }

    self.enumToString = function () {
        switch (self.question.questionType) {
            case 0: self.question.questionType = 'One possible answer'; break;
            case 1: self.question.questionType = 'Multiple answers'; break;
            case 2: self.question.questionType = 'Freeform answer'; break;
        }
    }

    self.stringToEnum = function () {
        switch (self.question.questionType) {
            case 'One possible answer': self.question.questionType = 0; break;
            case 'Multiple answers': self.question.questionType = 1; break;
            case 'Freeform answer': self.question.questionType = 2; break;
        }
    }

    self.getTest = function (id) {
        self.questionId = id;
        self.loading = true;
        self.question = {};
        $http({
            method: 'GET',
            url: apiUrl + 'api/Tests/' + id,

        }).then(function (resp) {
            self.test = resp.data;
            self.loading = false;
        });
    }


    self.getQuestion = function (id) {
        self.questionId = id;
        self.loading = true;
        self.question = {};
        $http({
            method: 'GET',
            url: apiUrl + 'api/Question/'+id,

        }).then(function (resp) {
            self.question = resp.data;
            self.enumToString();
            self.loading = false;
        });
    }

    self.saveTest = function () {
        $http({
            method: 'POST',
            url: apiUrl + 'api/Tests',
            data: self.test
        }).then(function (resp) {
            swal('Saved!');
            self.backToMenu();
        });
    }

    self.saveQuestion = function () {
        self.loading = true;
        self.stringToEnum();
        $http({
            method:'POST',
            url: apiUrl + 'api/Question/AddNewQuestion',
            data:self.question

        }).then(function (resp) {
            swal('Saved!');
            self.getQuestion(self.questionId);
        });
    }

    self.goToEdit = function (id) {
        $window.location.href = "/Questions/Edit/"+id;
    }

    self.goToHistory = function (id) {
        $window.location.href = "/Questions/History/" + id;
    }

    self.selectQuestionToAdd = function(id)
    {
        self.questionToAdd = id;
    }

    self.getAllQuestions = function () {
        self.loading = true;
        $http({
            method: 'GET',
            url: apiUrl + 'api/Questions',

        }).then(function (response) {
            self.questions = response.data;
            self.loading = false;
        })
    }

    self.getAllTests = function () {
        self.loading = true;
        $http({
            method: 'GET',
            url: apiUrl + 'api/Tests',

        }).then(function (response) {
            self.tests = response.data;
            self.loading = false;
        })
    }

    self.addToTest = function (tid) {
        self.loading = true;
        $http({
            method: 'POST',
            url: apiUrl + 'api/Tests/Add?qid='+self.questionToAdd+'&tid='+tid,

        }).then(function (response) {
            self.getTest(self.testId);
            self.loading = false;
        })
    }

    self.updateTest = function () {
        self.loading = true;
        $http({
            method: 'PUT',
            url: apiUrl + 'api/Tests/' + self.testId,
            data: self.test

        }).then(function (response) {
            self.getTest(self.testId);
            self.loading = false;
        })
    }

    self.removeQuestionFromTest = function (id) {
        self.loading = true;
        $http({
            method: 'DELETE',
            url: apiUrl + 'api/Tests?qid=' + id + '&tid=' + self.testId,

        }).then(function (response) {
            self.getTest(self.testId);
            self.loading = false;
        })
    }

    self.editQuestion = function (id) {
        self.loading = true;
        self.stringToEnum();
        $http({
            method: 'PUT',
            url: apiUrl + 'api/Question/EditQuestion/'+id,
            data: self.question

        }).then(function (resp) {
            swal('Saved!');
            self.getQuestion(id);
            self.loading = false;
        });
    }

    self.confirmDeletion = function (callback, id)
    {
        swal({
            title: "Are you sure?",
            text: "",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            cancelButtonColor: "#5EA1CE",
            confirmButtonText: "Yes",
            closeOnConfirm: true
        },function(){
            callback(id)
        });
    }



}
angular.module('confe')
 .controller('questionsController', questionsController);