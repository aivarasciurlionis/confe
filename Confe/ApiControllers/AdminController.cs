﻿using DataEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Confe.ApiControllers
{
    [Authorize(Roles="Admin")]
    public class AdminController : ApiController
    {
        DataContext db = new DataContext();
        [HttpGet]
        public IEnumerable<User> AllUsers()
        {
            return db.Users.ToList();
        }


    }
}
