﻿using DataEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Confe.ApiControllers
{
    [Authorize]
    public class TestsController : ApiController
    {

        private DataContext db = new DataContext();


        private User GetCurrentUser()
        {
            var UserId = User.Identity.GetUserId();
            return db.Users.Find(UserId);
        }

        [HttpGet, ActionName("All")]
        public IHttpActionResult GetAllTests()
        {
            var user = GetCurrentUser();
            return Ok(db.Tests.Where(t => t.User.Id == user.Id).ToList());
        }

        [HttpPut]
        public IHttpActionResult UpdateTest(int id, [FromBody]Test test)
        {

            if (test.Id != id)
                return BadRequest();
            try
            {
                db.Entry(test).State = System.Data.Entity.EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
            return Ok();
        }

        [HttpDelete, ActionName("Remove")]
        public IHttpActionResult RemoveTest(int id)
        {
            var user = GetCurrentUser();
            var test = db.Tests.Include("Questions").Single(t => t.Id == id);
            try
            {
                for (int i = 0; i < test.Questions.Count; i++)
                {
                    test.Questions.Remove(test.Questions.ElementAt(i));
                }
                db.Entry(test).State = System.Data.Entity.EntityState.Deleted;
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
            return Ok();
        }

        [HttpDelete, ActionName("RemoveQuestion")]
        public IHttpActionResult RemoveQuestionFromTest(int qid, int tid)
        {
            try
            {
                var question = db.Questions.Find(qid);
                var test = db.Tests.Find(tid);
                if (test.Questions.Contains(question))
                {
                    test.QuestionCount--;
                    test.Questions.Remove(question);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
            return Ok();
        }

        [HttpGet, ActionName("Single")]
        public IHttpActionResult GetSingleTest(int id)
        {
            var user = GetCurrentUser();
            var test = db.Tests.Include("Questions").Single(t => t.Id == id);
            return Ok(test);
        }

        [HttpPost, ActionName("New")]
        public IHttpActionResult NewTest(Test t)
        {
            var user = GetCurrentUser();
            user.Tests.Add(t);
            db.SaveChanges();
            return Ok(user.Id);
        }

        [Route("api/Tests/Add")]
        [HttpPost, ActionName("AdQuestion")]
        public IHttpActionResult AddQuestion(int qid, int tid)
        {
            try
            {
                var question = db.Questions.Find(qid);
                var test = db.Tests.Find(tid);

                if (!test.Questions.Contains(question))
                {
                    test.QuestionCount++;
                    test.Questions.Add(question);
                    db.SaveChanges();
                }
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
            return Ok();


        }

    }
}
