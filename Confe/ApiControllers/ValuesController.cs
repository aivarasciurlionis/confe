﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Tracing;

namespace Confe.Controllers
{
    public class ValuesController : ApiController
    {
         private ITraceWriter _tracer;
         public ValuesController()
         {
             _tracer = GlobalConfiguration.Configuration.Services.GetTraceWriter(); 
         }


        // GET api/values
        public IEnumerable<string> Get()
        {
            _tracer.Info(Request, this.ControllerContext.ControllerDescriptor.ControllerType.FullName, "Info Loaded: ");
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        public string Get(int id)
        {
            return "value";
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
