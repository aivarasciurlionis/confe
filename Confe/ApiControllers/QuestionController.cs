﻿using DataEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Confe.ApiControllers
{
     [Authorize]
    public class QuestionController : ApiController
    {


        DataContext db = new DataContext();
        [HttpGet]       
        public IHttpActionResult GetQuestion(int id)
        {
            var User = GetCurrentUser();
            Question question;
            try
            {
                question = db.Questions.Include("QuestionAnswers").First(q => q.Id == id);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }

            if (User.Questions.Contains(question))
            {
                return Ok(question);
            }
            else
            {
                return BadRequest("User can't access this question");
            }
        }

        [HttpGet]
        [Route("api/Questions")]
        public IHttpActionResult GetQuestions()
        {
            var User = GetCurrentUser();

            return Ok(db.Questions.Where(u => u.User.Id == User.Id).Select(q => new QuestionView {             
            AnswerCount = q.QuestionAnswers.Count(),
            CurrentAnswers = q.CurrentAnswers,
            LastAsked = q.LastAsked,
            TotalAnswers = q.TotalAnswers,
            Id = q.Id,
            QuestionName = q.QuestionName,
            QuestionType = q.QuestionType
            }
            ));
        }

        private bool UserHasQuestion(int id)
        {
            var UserId = User.Identity.GetUserId();
            return db.Questions.Where(u => u.User.Id == UserId).Any(q => q.Id == id);
        }

        private User GetCurrentUser()
        {
            var UserId = User.Identity.GetUserId();
            return db.Users.Find(UserId);
        }

        [HttpPut]
        [Route("api/Question/EditQuestion/{id}")]
        public IHttpActionResult EditQuestion(int id, Question Q)
        {        

            if (!UserHasQuestion(id))
            {
                return BadRequest("User can't access this question");
            }


            try
            {

                foreach (var a in Q.QuestionAnswers)
                {
                    if (a.Id == 0)
                        db.Entry(a).State = System.Data.Entity.EntityState.Added;
                    else
                    {
                        db.Entry(a).State = System.Data.Entity.EntityState.Modified;
                    }
                }

                db.Questions.Attach(Q);
                db.Entry(Q).State = System.Data.Entity.EntityState.Modified;

                var NewIds = Q.QuestionAnswers.Select(q => q.Id).ToList();
                var CurrentIds = db.Questions.Include("QuestionAnswers").Single(q => q.Id == id).QuestionAnswers.Select(a => a.Id).ToList();

                var ToRemove = CurrentIds.Except(NewIds).ToList();

                foreach (var q in ToRemove)
                {
                    db.Entry(db.QuestionAnswers.Find(q)).State = System.Data.Entity.EntityState.Deleted;
                }            

                db.SaveChanges();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
            return Ok();
        }

        [HttpDelete]
        public IHttpActionResult DeleteQuestion(int id)
        {
            var q = db.Questions.Include("QuestionAnswers").Single(k => k.Id == id);
            var User = GetCurrentUser();
            if (!User.Questions.Contains(q))
                return BadRequest("User can't access this question");

            foreach (var answ in q.QuestionAnswers.ToList())
            {
                db.Entry(answ).State = System.Data.Entity.EntityState.Deleted;
            }

            db.Entry(q).State = System.Data.Entity.EntityState.Deleted;


            db.SaveChanges();
            return Ok();

        }

        [HttpPost]
        public IHttpActionResult AddNewQuestion(Question Q)
        {
            var User = GetCurrentUser();
            try
            {
                User.Questions.Add(Q);
                db.SaveChanges();
            }
            catch (Exception e)
            {
                return InternalServerError(e);
            }
            return Ok();
        }
    }
}
