﻿using DataEntities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Security.Cryptography;
using System.Web.Http;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Text;
using Confe.Providers;

namespace Confe.ApiControllers
{
    [RoutePrefix("api/Conference")]
    public class ConferenceController : ApiController
    {

        DataContext db = new DataContext();

        [Authorize]
        [Route("UserDefaults")]
        public IHttpActionResult GetUserDefaults()
        {
            var User = GetCurrentUser();

            return Ok(new
            {
                ShowResultsForParticipants = User.ShowResultsForParticipants,
                DefaultConferenceTime = User.DefaultConferenceTime
            });
        }

        [Authorize]
        [HttpPut]
        [Route("UserDefaults")]
        public IHttpActionResult SaveUserDefaults(User defs)
        {
            var User = GetCurrentUser();

            User.DefaultConferenceTime = defs.DefaultConferenceTime;
            User.ShowResultsForParticipants = defs.ShowResultsForParticipants;
            db.SaveChanges();
            return Ok();
        }

        [Authorize]
        [Route("UniqueKey")]
        public IHttpActionResult GetUniqueKey()
        {
            int size = 8;
            string newKey = "";
            do
            {
                char[] chars = new char[62];
                chars =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
                byte[] data = new byte[1];
                using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
                {
                    crypto.GetNonZeroBytes(data);
                    data = new byte[size];
                    crypto.GetNonZeroBytes(data);
                }
                StringBuilder result = new StringBuilder(size);
                foreach (byte b in data)
                {
                    result.Append(chars[b % (chars.Length)]);
                }
                newKey = result.ToString();

            } while (Exists(newKey));

            var UserId = User.Identity.GetUserId();
            db.Users.Find(UserId).ConferenceKey = newKey;
            db.SaveChanges();
            return Ok(newKey);
        }

        private bool Exists(string key)
        {
            return db.Users.Any(u => u.ConferenceKey == key);
        }

        private User FindUserByKey(string key)
        {
            return db.Users.Single(u => u.ConferenceKey == key);
        }


        [HttpGet]
        [Route("Exists")]
        public IHttpActionResult KeyExists(string key)
        {
            if (Exists(key))
                return Ok();
            else
                return BadRequest("Conference key does not exist");
        }

        [HttpPost]
        [Route("SubmitAnswer")]
        public IHttpActionResult SubmitAnswer(string key, [FromBody]AnswerPrototype Answer)
        {
            Question Q;
            var result = CheckQuestionStatus(key, out Q);
            if (result != "")
                return BadRequest(result);
            else
            {
                var found = false;
                if (Q.QuestionType == Question.QuestionTyp.Single)
                    foreach (var qa in Q.QuestionAnswers)
                        if (qa.Id == Answer.SingleId)
                        {
                            qa.CurrentAnswersCount += 1;
                            found = true;
                        }

                if (Q.QuestionType == Question.QuestionTyp.Multiple)
                    foreach (var qa in Q.QuestionAnswers)
                        foreach (var a in Answer.MultipleIds)
                            if (qa.Id == a)
                            {
                                qa.CurrentAnswersCount += 1;
                                found = true;
                            }

                if (Q.QuestionType == Question.QuestionTyp.Freeform)
                {
                    foreach (var qa in Q.QuestionAnswers)
                        if (qa.Name == Answer.Freeform)
                        {
                            qa.CurrentAnswersCount += 1;
                            found = true;
                        }
                    if (!found)
                    {
                        Q.QuestionAnswers.Add(new QuestionAnswer { Name = Answer.Freeform, CurrentAnswersCount = 1 });
                        found = true;
                    }
                }

                if (found)
                    Q.CurrentAnswers += 1;
                else
                    return BadRequest("Wrong answer id/ids");

                db.SaveChanges();

                var User = FindUserByKey(key);
                if (User.ShowResultsForParticipants)
                    return Ok(true);
                else
                    return Ok(false);
            }
        }

        [Authorize]
        [HttpGet]
        [Route("CurrentAnswers")]
        public IHttpActionResult GetCurrentAnswers()
        {
            var User = GetCurrentUser();
            if (User.HasActiveQuestion && User.ActiveQuestionId > 0)
            {
                return Ok(db.Questions.Find(User.ActiveQuestionId).CurrentAnswers);
            }
            else return BadRequest("User has no active question");
        }

        public string CheckQuestionStatus(string key, out Question Q)
        {
            Q = null;
            if (Exists(key))
            {
                var CurrentTime = DateTime.UtcNow;
                var User = FindUserByKey(key);
                if (User.HasActiveQuestion && User.ActiveQuestionId > 0)
                {
                    var Question = db.Questions.Include("QuestionAnswers").First(q => q.Id == User.ActiveQuestionId);
                    if (Question.Expires > CurrentTime)
                    {
                        Q = Question;
                        return "";
                    }
                    else
                        return "Question has expired";
                }
                else
                    return "No active questions";
            }
            else
                return "Conference key has expired";


        }


        [HttpGet]
        [Route("ActiveQuestions")]
        public IHttpActionResult CheckForActiveQuestions(string key)
        {
            Question Q;
            var result = CheckQuestionStatus(key, out Q);
            if (result != "")
                return Ok(result);
            else
            {
                return Ok(Q);
            }
        }


        private User GetCurrentUser()
        {
            var UserId = User.Identity.GetUserId();
            return db.Users.Find(UserId);
        }

        private bool UserHasQuestion(int id)
        {
            var UserId = User.Identity.GetUserId();
            return db.Questions.Where(u => u.User.Id == UserId).Any(q => q.Id == id);
        }


        [Authorize]
        [HttpGet]
        [Route("ResultsUser")]
        public IHttpActionResult GetResultsUser()
        {
            var User = GetCurrentUser();
            if (User.ShowingResults && User.ResultQuestionId > 0)
            {
                return Ok(db.Questions.Include("QuestionAnswers").Single(q => q.Id == User.ResultQuestionId));
            }
            else
                return BadRequest("No results available");
        }

        [HttpGet]
        [Route("ResultsAnon")]
        public IHttpActionResult GetResultsAnon(string key)
        {
            if (Exists(key))
            {
                var User = FindUserByKey(key);
                if (User.ShowResultsForParticipants)
                {
                    if (User.ShowingResults && User.ResultQuestionId > 0)
                    {
                        return Ok(db.Questions.Include("QuestionAnswers").Single(q => q.Id == User.ResultQuestionId));
                    }
                    else
                        return Ok("No results available");
                }
                else
                    return BadRequest("User is not sharing question results");
            }
            else
                return BadRequest("Conference key has expired");
        }


        [Authorize]
        [Route("MakeActive")]
        [HttpPost]
        public IHttpActionResult MakeQuestionActive(int id)
        {
            if (!UserHasQuestion(id))
                return BadRequest("User can't access this question");

            DeactivateLastQ();
            PassResultsToHistory();

            var User = GetCurrentUser();

            User.ShowingResults = false;
            User.ResultQuestionId = 0;
            User.HasActiveQuestion = true;
            User.ActiveQuestionId = id;
            db.SaveChanges();
            var Question = db.Questions.Find(id);
            Question.LastAsked = DateTime.UtcNow;
            Question.Expires = Question.LastAsked.Value.AddSeconds(User.DefaultConferenceTime);
            db.SaveChanges();
            return Ok();
        }

        private void PassResultsToHistory()
        {
            var User = GetCurrentUser();
            if (User.HasActiveQuestion || User.ShowingResults)
            {
                Question Question = null;
                if (User.HasActiveQuestion)
                 Question = db.Questions.Include("QuestionAnswers").Where(q => q.User.Id == User.Id).First(qq => qq.Id == User.ActiveQuestionId);
                else if (User.ShowingResults)
                    Question = db.Questions.Include("QuestionAnswers").Where(q => q.User.Id == User.Id).First(qq => qq.Id == User.ResultQuestionId);


                Question.TotalAnswers += Question.CurrentAnswers;
                Question.CurrentAnswers = 0;

                foreach (var Q in Question.QuestionAnswers)
                {
                    Q.TotalAnswersCount += Q.CurrentAnswersCount;
                    Q.CurrentAnswersCount = 0;
                }

                db.SaveChanges();
            }
        }

        private void DeactivateLastQ()
        {
            var User = GetCurrentUser();
            if (User.HasActiveQuestion)
            {
                User.HasActiveQuestion = false;
                User.ShowingResults = true;           
                User.ResultQuestionId = User.ActiveQuestionId;
                var Question = db.Questions.Find(User.ActiveQuestionId);
                Question.Expires = DateTime.UtcNow;
                User.ActiveQuestionId = 0;
                db.SaveChanges();
            }
        }


        [Authorize]
        [Route("DeactivateLast")]
        [HttpPost]
        public IHttpActionResult DeactivateLastQuestion()
        {
            DeactivateLastQ();
            return Ok();
        }



    }
}
