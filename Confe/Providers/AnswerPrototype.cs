﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Confe.Providers
{
    public class AnswerPrototype
    {
        public  int  QuestionId{ get; set; }
        public int SingleId { get; set; }
        public IList<int> MultipleIds { get; set; }
        public string Freeform { get; set; }
    }
}